# RENDALO - APP

## Requirements

- A running instance of Rendalo API v1

## Setting Up

### Install Dependencies

```bash
yarn
```

### Environment Variables

Create a file, `.env`, in the project root that contains the base url of the API.

```bash
# .env

REACT_APP_API_BASE_URL=http://localhost:8000/api/v1
```

## Up and Running

### Run

```bash
yarn local
```

### Access

```bash
http://localhost:3000/
```

## Testing

### Unit Tests

TODO
