import React from 'react';
import PropTypes from 'prop-types';
import './Input.css';

/**
 * The Input's component.
 *
 * @property    {string}      [label=null]        -- The input's label.
 * @property    {string}      [placeholder='']    -- The input's placeholder.
 * @property    {string}      [type='text']       -- The input's type.
 * @property    {string}      value               -- The input's value.
 * @property    {Function}    onChange            -- The change event handler.
 */
function Input(props) {
  const { label = null, placeholder = '', type = 'text', value, onChange } = props;

  return (
    <div className="Input">
      {label && <label>{label}</label>}
      <input 
        className="Input-field" 
        placeholder={placeholder}
        type={type} 
        value={value} 
        onChange={onChange}
      />
    </div>
  );
}

Input.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.oneOf(['text', 'number']),
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Input;