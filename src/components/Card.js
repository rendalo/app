import React from 'react';
import PropTypes from 'prop-types';
import './Card.css';

/**
 * The Card's component.
 *
 * @property    {string}    description   -- The card's description.
 * @property    {number}    price         -- The card's price.
 * @property    {string}    title         -- The card's title.
 */
function Card(props) {
  const { description, price, title } = props;

  return (
    <div className="Card">
      <h1>{title}</h1>
      <p>{description}</p>
      <h2>{`$${price}`}</h2>
    </div>
  );
}

Card.propTypes = {
  description: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
};

export default Card;