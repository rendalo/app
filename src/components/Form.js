import React from 'react';
import PropTypes from 'prop-types';
import './Form.css';

/**
 * The Form's component.
 *
 * @property    {ReactNode}   children      -- The children.
 * @property    {Function}    [onSubmit]    -- The submit event handler.
 */
function Form(props) {
  const { children, onSubmit = undefined } = props;

  return (
    <form className="Form" onSubmit={onSubmit}>
      {children}
    </form>
  );
}

Form.propTypes = {
  children: PropTypes.node.isRequired,
  onSubmit: PropTypes.func,
};

export default Form;