import React from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';
import './Modal.css';

/**
 * The Modal's component.
 *
 * @property    {ReactNode}   children    -- The children.
 * @property    {boolean}     show        -- The show or hide the modal.
 */
function Modal(props) {
  const element = document.getElementById('__modal');
  const { children, show = false } = props;
  const content = (
    <div className="Modal">
      {children}
    </div>
  );

  if (!element || !show) {
    return null;
  }

  return createPortal(content, element);
}

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  show: PropTypes.bool.isRequired,
};

export default Modal;