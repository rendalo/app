import React from 'react';
import PropTypes from 'prop-types';
import './TextArea.css';

/**
 * The TextArea's component.
 *
 * @property    {string}      [label=null]    -- The text area's label.
 * @property    {string}      value           -- The text area's value.
 * @property    {Function}    onChange        -- The change event handler.
 */
function TextArea(props) {
  const { label = null, value = '', onChange } = props;

  return (
    <div className="TextArea">
      {label && <label>{label}</label>}
      <textarea className="TextArea-field" value={value} onChange={onChange} />
    </div>
  );
}

TextArea.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default TextArea;