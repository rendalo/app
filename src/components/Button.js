import React from 'react';
import PropTypes from 'prop-types';
import './Button.css';

/**
 * The Button's component.
 *
 * @property    {ReactNode}   children            -- The children.
 * @property    {string}      [skin='primary']    -- The button's skin.
 * @property    {string}      [type='button']     -- The button's type.
 * @property    {Function}    [onClick]           -- The click event handler.
 */
function Button(props) {
  const { children: text, skin = 'primary', type = 'button', onClick = undefined } = props;
  const buttonClass = `Button Button-${skin}`;

  return (
    <button 
      className={buttonClass} 
      type={type} 
      onClick={onClick}
    >
      {text}
    </button>
  );
}

Button.propTypes = {
  children: PropTypes.string.isRequired,
  skin: PropTypes.oneOf(['primary', 'secondary', 'cancel']),
  type: PropTypes.oneOf(['button', 'submit']),
  onClick: PropTypes.func,
};

export default Button;