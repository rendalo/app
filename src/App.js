import React, { useEffect, useState } from 'react';
import { addAd, getAds } from './services/ads';
import Modal from './components/Modal';
import Form from './components/Form';
import Input from './components/Input';
import TextArea from './components/TextArea';
import Button from './components/Button';
import Card from './components/Card';
import './App.css';

/**
 * Helper to update the collection state.
 * 
 * @param   {SetStateAction}    setCollection   -- The set collection state.
 * @returns {void}
 */
const fetchAds = async (setCollection) => {
  const ads = await getAds();

  setCollection(ads);
};

/**
 * The App's root component.
 */
function App() {
  const [collection, setCollection] = useState([]);
  const [showForm, setShowForm] = useState(false);
  const [formError, setFormError] = useState('');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [rent, setRent] = useState('');
  const handleShowForm = (show) => () => {
    setShowForm(show);
  };
  const handleOnSubmit = (event) => {
    event.preventDefault();

    addAd({ description, name, rent })
      .then(() => {
        fetchAds(setCollection);
        setShowForm(false);
        setFormError('');
        setName('');
        setDescription('');
        setRent('');
      })
      .catch(error => setFormError(error.toString()));
  };
  const handleOnChangeName = (event) => setName(event.currentTarget.value);
  const handleOnChangeDescription = (event) => setDescription(event.currentTarget.value);
  const handleOnChangeRent = (event) => setRent(event.currentTarget.value);

  useEffect(() => {
    fetchAds(setCollection);
  }, []);

  return (
    <div className="App">
      <header>
        <Button onClick={handleShowForm(true)}>Publicar aviso</Button>
        <Modal show={showForm}>
          <Form onSubmit={handleOnSubmit}>
            <section>
              <h1>PUBLICAR AVISO</h1>
              {formError !== '' && <p className="App-error">{formError}</p>}
              <Input label="Nombre" value={name} onChange={handleOnChangeName} />
              <TextArea label="Descripción" value={description} onChange={handleOnChangeDescription} />
              <Input label="Valor arriendo" type="number" value={rent} onChange={handleOnChangeRent} />
            </section>
            <Button skin="secondary" type="submit">Publicar</Button>
            <Button skin="cancel" onClick={handleShowForm(false)}>Cancelar</Button>
          </Form>
        </Modal>
      </header>
      <main>
        {collection.reverse().map((ad = {}) => 
          <Card key={ad._id} description={ad.description} price={ad.rent} title={ad.name} />
        )}
      </main>
    </div>
  );
}

export default App;
