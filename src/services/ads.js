const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;

/**
 * To get a collection of all available ads.
 *
 * @export
 * @returns {Promise<Ad[]>}   The ads' collection.
 */
export function getAds() {
  return fetch(`${API_BASE_URL}/ads`)
    .then(response => {
      return response.json();
    });
}

/**
 * To create a new ad.
 *
 * @export
 * @param     {Ad} [payload={}]   -- The payload to create a new ad.
 * @returns   {Promise<void>}
 */
export function addAd(payload = {}) {
  return fetch(`${API_BASE_URL}/ad`, {
    body: JSON.stringify(payload),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    mode: 'cors',
    referrer: 'no-referrer',
  })
  .then(response => {
    if (response.status !== 201) {
      throw new Error('Algo salió mal. Intenta de nuevo más tarde.');
    }
  });
}